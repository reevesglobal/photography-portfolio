document.getElementById("overlay").addEventListener("click", function (e) {
    this.style.display = "none";
});

document.querySelector(".photogallery").addEventListener("click", function (e) {
    if (e.target.src) {
        document.getElementById("overlayImg").src = e.target.src;
        document.getElementById("imgCaption").innerHTML = e.target.alt;
        document.getElementById("overlay").style.display = "block";
    }
});